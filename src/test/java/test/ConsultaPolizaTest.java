package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.FixMethodOrder;
import pages.ConsultaPolizaPage;
import org.junit.rules.TestName;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import util.CapturarPantalla;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ConsultaPolizaTest {		
	
		//Driver
		WebDriver driver;	
		ConsultaPolizaPage consultaPoliza;
		File screenshot;
		CapturarPantalla captura;
		
		@Rule
		public TestName testName = new TestName();
				
		@After
		public void cerrardriver() throws IOException {
			//driver.quit();
		}
		
		@Test
		public void ConsultaPolizaTest() throws IOException, InterruptedException {		
			
			// lee el archivo properties e inicializa el Driver
	        Properties prop = new Properties();
	        prop.load(new FileInputStream("rutas.properties"));
	        System.setProperty("webdriver.chrome.driver", prop.getProperty("chromeDriver"));
			System.out.println("Ejecutando TEST: " + testName.getMethodName());
			
			captura = new CapturarPantalla();
			String patente="";
			String csvFile = prop.getProperty("BusquedaPatente");
			BufferedReader br = null;
			String line = "";
			//Se define separador ","
			String cvsSplitBy = ",";
			try {
			    br = new BufferedReader(new FileReader(csvFile));
			    while ((line = br.readLine()) != null) {                
			        String[] datos = line.split(cvsSplitBy);
			        //Imprime datos.
					patente = datos[0];
				    Thread.sleep(2000);		
				    
				    
				    driver = new ChromeDriver();
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					
					//Ingresar a la Página					
					driver.get(prop.getProperty("urlCardif"));
					String urlname = driver.getCurrentUrl();
					System.out.println("Se ha ingresado al sitio:" + urlname);		
					Thread.sleep(5000);	
					
					driver.findElement(By.linkText("Seguros")).click();	
					driver.findElement(By.linkText("Seguro Obligatorio de Accidentes Personales SOAP BNP Paribas Cardif")).click();
					driver.findElement(By.linkText("CONSULTA TU PÓLIZA AQUÍ")).click();
					consultaPoliza = new ConsultaPolizaPage(driver);
					ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
				    driver.switchTo().window(tabs2.get(1));
					consultaPoliza.ingresarPatente(patente);
					driver.findElement(By.xpath("//*[@id='policyForm']/div[1]/div[1]/div/div/div[2]/div/a[2]")).click();
					Thread.sleep(2000);	
					captura.takeScreenshot(driver, "/ConsultarPatente");	
					System.out.println("Consulta realizada");
					
					driver.close();
					driver.switchTo().window(tabs2.get(0));
					driver.close();
					
			     }
			    } catch (Exception e) {
				System.out.println("Ocurrio un problema inexperado ");
			}
		}
}


