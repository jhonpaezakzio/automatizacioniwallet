package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.junit.FixMethodOrder;
import pages.LoginPage;
import org.junit.rules.TestName;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import util.CapturarPantalla;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class LoginTest {		
	
		//Driver
		WebDriver driver;	
		LoginPage login;
		File screenshot;
		CapturarPantalla captura;
		
		@Rule
		public TestName testName = new TestName();
				
		@After
		public void cerrardriver() throws IOException {
			//driver.quit();
		}
		
		@Test
		public void LoginTest() throws IOException, InterruptedException {		
			
			// lee el archivo properties e inicializa el Driver
	        Properties prop = new Properties();
	        prop.load(new FileInputStream("rutas.properties"));
	        //System.setProperty("webdriver.gecko.driver", prop.getProperty("firefoxDriver"));
	        System.setProperty("webdriver.chrome.driver", prop.getProperty("chromeDriver"));
			System.out.println("Ejecutando TEST: " + testName.getMethodName());

			
			captura = new CapturarPantalla();
			String tipoDoc="", numDoc="", contrasena="";
			String csvFile = prop.getProperty("Login");
			BufferedReader br = null;
			String line = "";
			//Se define separador ","
			String cvsSplitBy = ",";
			try {
			    br = new BufferedReader(new FileReader(csvFile));
			    while ((line = br.readLine()) != null) {                
			        String[] datos = line.split(cvsSplitBy);
			        //Imprime datos.
					tipoDoc = datos[0];
					numDoc = datos[1];
					contrasena = datos[2];
				    Thread.sleep(2000);		

				    
				    driver = new ChromeDriver();
				    //driver = new FirefoxDriver();
					driver.manage().window().maximize();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					
					//Ingresar a la Página		
					//driver.get("https://www.google.cl");
					driver.get(prop.getProperty("urlLogin"));
					String urlname = driver.getCurrentUrl();
					System.out.println("Se ha ingresado al sitio:" + urlname);	
					Thread.sleep(2000);	
					
					//driver.findElement(By.id("txtNumberDocument")).sendKeys(numDoc);
					
					login = new LoginPage(driver); 
					
					login.seleccionaTipoDoc();
					
					if (tipoDoc.equals("RUT")){
						// Selecciona el tipo de documento "RUT"
						login.getRUTSelect().click();;
						Thread.sleep(3000);	
						
						} else if(tipoDoc.equals("DNI")){
							//Selecciona el tipo de documento "DNI"
							login.getDNISelect().click();
							} else {
								System.out.println("No Existe el tipo de documento");
						}	

					login.ingresarUsuario(numDoc, contrasena);
					login.ingresar();
					Thread.sleep(2000);	
					captura.takeScreenshot(driver, "/Login/Login");	
					System.out.println("Proceso de login finalizado");

					driver.close();
					
			   }
			   } catch (Exception e) {
				System.out.println("Ocurrio un problema inexperado ");
			}
		}
}


