package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
	
	public WebDriver driver;	
	
	@FindBy(id="cmbTypeDocument")
	private WebElement tipoDocSelect;
	
	@FindBy(id="md-option-3")
	private WebElement RUTSelect;
	
	@FindBy(id="md-option-2")
	private WebElement DNISelect;
	
	@FindBy(id="txtNumberDocument")
	private WebElement numDocText;
	
	@FindBy(id="txtPassword")
	private WebElement contrasenaText;
	
	@FindBy(id="btnLogin")
	private WebElement ingresarBtn;
	
	//Constructor de la Clase		
	public LoginPage(WebDriver driver){
		this.driver = driver;		
		PageFactory.initElements(driver, this); 
	}
	
	public WebElement getRUTSelect() {
		System.out.println("Seleccionando tipo de documento RUT");	
		return RUTSelect;
	}	
	
	public WebElement getDNISelect() {
		System.out.println("Seleccionando tipo de documento DNI");	
		return DNISelect;
	}	
	
	public void seleccionaTipoDoc(){				
		//Ingreso de numero de documento y contraseña
		System.out.println("selecciona campo tipo de documento");	
		tipoDocSelect.click();
	}
	
	public void ingresarUsuario(String numDoc, String contrasena){				
		//Ingreso de numero de documento y contraseña
		System.out.println("Ingresando numero de documento y contraseña");	
		numDocText.clear();
		numDocText.sendKeys(numDoc);
		contrasenaText.clear();
		contrasenaText.sendKeys(contrasena);
	}
	
	public void ingresar(){
		ingresarBtn.click();
	}
	
	public void ingresarDatoListaDesplegable(WebElement elemento, String dato) {
		Select drpDwnBox = new Select(elemento);
		drpDwnBox.selectByVisibleText(dato);
	}	
	
	public void checkAlert() {
	    try {
	        WebDriverWait wait = new WebDriverWait(driver, 2);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        System.out.println("Alerta: "+alert.getText());
	        alert.accept();	 	        
	    } catch (Exception e) {
	    	System.out.println("Se ha generado un problema con la Alerta");
		    }			
	}
}