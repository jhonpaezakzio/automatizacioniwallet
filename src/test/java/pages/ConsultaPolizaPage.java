package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ConsultaPolizaPage {
	
	public WebDriver driver;	
	
	/*@FindBy(xpath="//*[@id=\'nav\']/ul/li[3]/div/ul/li[3]/a")
	private WebElement seguroObligatorioBtn;
	
	@FindBy(xpath="//*[@id=\"rspeak\"]/div[2]/h4/a" )
	private WebElement consultaPolizaBtn;*/
	
	@FindBy(id="policyPlate")
	private WebElement ingresarPatenteText;
	
	@FindBy(xpath="//*[@id=\"policyForm\"]/div[1]/div[1]/div/div/div[2]/div/a[2]")
	private WebElement consultarBtn;
	
	public ConsultaPolizaPage(WebDriver driver){
		this.driver = driver;		
		PageFactory.initElements(driver, this); 
	}
	
	public void ingresarPatente(String patente){				
		//Ingreso de patente
		ingresarPatenteText.clear();
		ingresarPatenteText.sendKeys(patente);
	}
	
	
	public void consultar(){
		consultarBtn.click();
	}
	
}