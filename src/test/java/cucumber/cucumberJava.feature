@tag
Feature: Presentar los siniestros radicados, Una persona registrada como cliente desea ver sus siniestros radicados
@tag1
Scenario: Un Cliente desea ver sus siniestros radicados
Given La persona posee siniestros radicados
And El servicio de consulta de siniestros funcionando
And El usuario posee usuario y contraseña para el acceso al sistema
When El usuario se haya logueado exitosamente
And Presiona el botón de consultar siniestro
Then  Muestra la lista de siniestros
And Deben estar ordenados en el orden: fecha de cambio de estado, de menor fecha a mayor, fecha de radicación, Nro de siniestro