package testsuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.ConsultaPolizaTest;


@RunWith(Suite.class)
@SuiteClasses({
	ConsultaPolizaTest.class,

	})

public class TestSuite {

}